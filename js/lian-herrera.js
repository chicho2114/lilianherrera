$(document).ready(function(){       
   var scroll_start = 0;
   var startchange = $('#myCarousel');
   var offset = startchange.offset();
    if (startchange.length){
     $(document).scroll(function() { 
        scroll_start = $(this).scrollTop();
        if(scroll_start > offset.top) {
            $(".navbar-fixed-top").addClass('top-bar-change');
            $('.navbar-fixed-top').removeClass('top-bar-custom');
         } else {
            $('.navbar-fixed-top').removeClass('top-bar-change');
            $(".navbar-fixed-top").addClass('top-bar-custom');
         }
     });
    }
  $('#myCarousel').carousel({
    interval: 4000
  });
  $('#myCarouselTweets').carousel({
    interval: 40000
  });
});

$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 64)
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

function toggleImage(identificador){
  var imagen = $(identificador);
  
  imagen.toggleClass('imagen-bottom-div');
}

$("#contactForm").on('submit', function(e) {
            
    var $form = $(this);
    var formData = new FormData($form[0]);

    $.ajax({
        url: $form.attr('action'),
        type: 'POST',
        data: formData,
        statusCode: {
            200: function() {
                $("#contactForm").trigger('reset');
                alert('Mensaje enviado satisfactoriamente');
            },
            403: function()  {
                alert('Falló al enviar mensaje');
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
});