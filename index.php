<!DOCTYPE html>
<html lang="es">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>Lilian Herrera</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/jquery-min.css">
	<link rel="stylesheet" type="text/css" href="css/liam-herrera.css">
	<link rel="stylesheet" type="text/css" href="css/fonts.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/galeria.css">

	<!-- Captcha -->
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

</head>
<body>

<div class="navbar-wrapper">
  <div class="container">
    <div id="mainNav" class="navbar navbar-inverse navbar-fixed-top top-bar-custom">
      
        <div class="navbar-header">
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
        <a class="navbar-brand hidden-lg hidden-md hidden-sm page-scroll" href="#mainNav">
        	<span>
        		<img src="images/logo-liam-herrera.png" height="50">
        	</span>
        </a>
        </div>
        <div style="z-index: 16;" class="navbar-collapse collapse">
          <ul class="nav navbar-nav nav-list-elements-custom font-regular">
            <li class="menu-option-custom"><a class="page-scroll" href="#section-first">TRAYECTORIA</a></li>
            <li class="menu-option-custom hidden-sm hidden-xs">.</li>
            <li class="menu-option-custom"><a class="page-scroll" href="#section-two">ESTUDIOS REALIZADOS</a></li>
            <li class="menu-option-custom hidden-sm hidden-xs"><img id="logo-navbar" src="images/logo-liam-herrera.png"></li>
            <li class="menu-option-custom"><a class="page-scroll" href="#section-galeria">GALERIA</a></li>
            <li class="menu-option-custom hidden-sm hidden-xs">.</li>
            <li class="menu-option-custom"><a class="page-scroll" href="#section-contacto">CONTACTO</a></li>
          </ul>
        </div>

    </div>
  </div><!-- /container -->
</div><!-- /navbar wrapper -->


<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="item active">
      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
      <img style="width: 100%;" src="./images/carrusel-1-1280x720.jpg" class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          </div>
      </div>
    </div>
    <div class="item">
      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
      <img style="width: 100%;" src="./images/carrusel-2-1280x720.jpg" class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          
        </div>
      </div>
    </div>
    <div class="item">
      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
      <img style="width: 100%;" src="./images/carrusel-3-1280x720.jpg" class="img-responsive">
      <div class="container">
        <div class="carousel-caption">
          
        </div>
      </div>
    </div>
  </div>
 
</div>
<!-- /.carousel -->

<div id="section-first" class="col-lg-12" style="background-color: #f7f7f7; min-height: 250px;">
	<div class="container padding-vertical">
		<p class="text-center font-regular lead">TRAYECTORIA</p>
		<p class="text-center font-regular text-muted parrafo">Para la Dra. Lilian Herrera, cada paciente y cada caso representan un nuevo desafío en la búsqueda de nuevas técnicas que aporten mejores resultados, formas más armoniosas y naturales, y muchas satisfacciones, tanto para ella como para su equipo de trabajo. Y por supuesto para sus pacientes quienes siempre han sido su mejor herramienta de publicidad y carta de presentación. Basta con pasar un rato en la sala de espera de su consultorio para comprobar el alto grado de satisfacción y agradecimiento que manifiestan sus pacientes.</p>
	</div>
</div>

<div class="clearfix"></div>

<div id="section-two" class="col-lg-12 no-padding" style="background-color: #ffffff;overflow: hidden;
    position: relative;
    width: 100%;">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 contenedor-imagenes" style="background-color: red;">
      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
      <img style="width: 100%;" src="./images/imagen-2-640x480.jpg" class="img-responsive">
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 div-with-content-right"  style="">
		<div class="font-regular padding-vertical">
			<p class="text-center font-regular lead">ESTUDIOS REALIZADOS</p>

			<div class="padding-horizontal">
			<ul>
				<li class="text-muted">Hospital Central de Maracay (Cirujano Plástico 2005)</li>
				<li class="text-muted">Ciudad Hospitalaria "Dr. Enrique Tejera" (Cirujano General 2002)</li>
				<li class="text-muted">Universidad de Carabobo (Médico Cirujano 1996)</li>
				<li class="text-muted">MSDS: 52249. CM: Aragua:7647. Carabobo:5981.</li>
				<div id="collapse1" class="collapse">

				<li class="text-muted">Hospital Central de Maracay (Cirujano Plástico 2005)</li>
				<li class="text-muted">Ciudad Hospitalaria "Dr. Enrique Tejera" (Cirujano General 2002)</li>
				<li class="text-muted">Universidad de Carabobo (Médico Cirujano 1996)</li>
				<li class="text-muted">MSDS: 52249. CM: Aragua:7647. Carabobo:5981.</li>

				</div>
				<br>
				<div class="text-center">
					<span style="cursor: pointer;" onclick="$('#imagen-bottom-1').fadeToggle('slow');" data-parent="#accordionPrueba" data-toggle="collapse" data-target="#collapse1">- <i class="fa fa-plus-circle" style="font-size: 18px;" aria-hidden="true"></i> -</span>
				</div>				
			</ul>
			</div>
		</div>
		<div id="imagen-bottom-1" class="no-padding hidden-md">
			<!-- <img style="width: 100%;" class="img-responsive" src="./images/image-botton-section-1.jpg"> -->
			<img style="width: 100%;" class="img-responsive" src="http://ximg.es/600x120/ddd/000">
		</div>
	</div>
</div>

<div class="clearfix"></div>

<div class="col-lg-12 no-padding" style="background-color: #ffffff;">
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 div-with-content-left" style="">
		<div class="padding-vertical padding-horizontal">
				<p class="text-center font-regular lead">PROCEDIMIENTOS QUIRURGICOS</p>
				<p class="font-regular"><strong>Mamoplastia de Aumento</strong></p>
				<p class="font-regular text-muted text-justify">Una oportunidad al alcance de casi todas las mujeres, es el aumento de senos con una operación que permite mejorar la figura y con ello devolverle el nivel de autoestima que el tiempo y la maternidad a veces se lleva consigo.</p>

				

			<div id="collapse2" class="collapse">

				<p class="font-regular"><strong>Mamoplastia de Aumento</strong></p>
				<p class="font-regular text-muted text-justify">Una oportunidad al alcance de casi todas las mujeres, es el aumento de senos con una operación que permite mejorar la figura y con ello devolverle el nivel de autoestima que el tiempo y la maternidad a veces se lleva consigo.</p>

			</div>
			<div class="text-center">
				<span style="cursor: pointer;" onclick="$('#imagen-bottom-2').fadeToggle('slow');" data-toggle="collapse" data-target="#collapse2">- <i class="fa fa-plus-circle" style="font-size: 18px;" aria-hidden="true"></i> -</span>		
			</div>
		</div>
		<div id="imagen-bottom-2" class="no-padding hidden-md">
			<!-- <img style="width: 100%;" class="img-responsive" src="./images/image-botton-section-2.jpg"> -->
			<img style="width: 100%;" class="img-responsive" src="http://ximg.es/600x120/ddd/000">
		</div>
	</div>
	<div id="imagen-section-two" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 contenedor-imagenes">
      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
      <img style="width: 100%;" src="./images/imagen-3-640x480.jpg" class="img-responsive">
	</div>
</div>

<div class="clearfix"></div>

<div class="col-lg-12 no-padding" style="background-color: #ffffff;">
	
	<div class="col-lg-4 col-md-4 col-sm-12 contenedor-imagenes hidden-sm hidden-xs">
      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
      <img style="width: 100%;" src="./images/imagen-4-400x580.jpg" class="img-responsive">
	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 padding-vertical div-with-content-center" style="">
			<p class="text-center font-regular lead">CURSOS Y CONGRESOS</p>
		<ul>
			<li class="font-regular text-muted">XXXV Congreso Nacional de la SCCP 2015 Cartagena Colombia.</li>
			<li class="font-regular text-muted">Baker Gordon Educational Symposium The American Society for Aestbetic Plastic Surgery 2014 Miami, Florida.</li>
			<li class="font-regular text-muted">XX Congreso de la Federación Ibero Latinoamericana de Cirugía Plástica 2014 Cancun Q. Roo.</li>
			<div id="collapse3" class="collapse">

			<li class="font-regular text-muted">XX Congreso de la Federación Ibero Latinoamericana de Cirugía Plástica 2014 Cancun Q. Roo.</li>
			<li class="font-regular text-muted">XX Congreso de la Federación Ibero Latinoamericana de Cirugía Plástica 2014 Cancun Q. Roo.</li>
			</div>
			<br>
			<div class="text-center">
				<span style="cursor: pointer;" onclick="$('#imagen-bottom-3').fadeToggle('slow');" class="text-center" data-toggle="collapse" data-target="#collapse3">- <i class="fa fa-plus-circle" style="font-size: 18px;" aria-hidden="true"></i> -</span>
			</div>
			<br>

		</ul>

		<div id="imagen-bottom-3" class="no-padding" style="">
			<!-- <img style="width: 100%;" class="img-responsive" src="./images/image-botton-section-3.jpg"> -->
			<img style="width: 100%;" class="img-responsive" src="http://ximg.es/400x120/ddd/000">
		</div>
	</div>
	<div id="imagen-left-section-3" class="col-lg-4 col-md-4 col-sm-12 contenedor-imagenes">
      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
      <img style="width: 100%;" src="./images/imagen-5-400x580.jpg" class="img-responsive">	
	</div>
</div>

<div class="clearfix"></div>
<?php	
	date_default_timezone_set('America/Caracas');
	require_once('tweets.php');

	$twitterObject = new Twitter();
	$tweets = $twitterObject->PayloadTweets();

?>
<div class="col-lg-12" style="background-color: #f5f5f5; min-height: 350px;">
	<div class="text-center">
		<div id="myCarouselTweets" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators" >
		    <li style="border: 1px solid black;" data-target="#myCarouselTweets" data-slide-to="0" class="active"></li>
		    <li style="border: 1px solid black;" data-target="#myCarouselTweets" data-slide-to="1"></li>
		    <li style="border: 1px solid black;" data-target="#myCarouselTweets" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <div class="item active">
		    	<div class="padding-horizontal padding-vertical">
					<div class="text-center">
				      	<i class="fa fa-twitter fa-2x" style="color: #2290bf;" aria-hidden="true"></i>
						<p class="font-regular"><?php echo $tweets[0]['name_user'] ?>  <?php echo $tweets[0]['screen_name'] ?><span class="text-muted">/ <?php echo date("d-m-Y H:ia", strtotime($tweets[0]['fecha'])); ?></span></p>
						<p class="text-muted" style="font-size: 18px;"><?php echo $tweets[0]['tweet'] ?>.</p>
			    	</div>
			  	</div>
			</div>


		    <div class="item">
		    	<div class="padding-horizontal padding-vertical">
					<div class="text-center">
				      	<i class="fa fa-twitter fa-2x" style="color: #2290bf;" aria-hidden="true"></i>
						<p class="font-regular"><?php echo $tweets[1]['name_user'] ?>  <?php echo $tweets[1]['screen_name'] ?><span class="text-muted">/ <?php echo date("d-m-Y H:ia", strtotime($tweets[1]['fecha'])); ?></span></p>
						<p class="text-muted" style="font-size: 18px;"><?php echo $tweets[1]['tweet'] ?>.</p>
		    		</div>
		  		</div>
			</div>


		    <div class="item">
		    	<div class="padding-horizontal padding-vertical">
					<div class="text-center">
				      	<i class="fa fa-twitter fa-2x" style="color: #2290bf;" aria-hidden="true"></i>
						<p class="font-regular"><?php echo $tweets[2]['name_user'] ?>  <?php echo $tweets[2]['screen_name'] ?><span class="text-muted">/ <?php echo date("d-m-Y H:ia", strtotime($tweets[2]['fecha'])); ?></span></p>
						<p class="text-muted" style="font-size: 18px;"><?php echo $tweets[2]['tweet'] ?>.</p>
		    		</div>
		  		</div>
			</div>

		  </div>
		</div>
	</div>
</div>

<div class="clearfix"></div>
<div id="section-galeria" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" style="background-color: #ffffff; /*min-height: 250px;*/">
	<div class="text-center padding-vertical" style="background-color: #676767;">
		<button id="button-galeria" class="btn btn-lg font-regular btn-block">GALERÍA</button>
	</div>
	<div class="clearfix"></div>
	<div style="overflow-x: auto; white-space: nowrap;">
		<div style="display: inline-block; float: none;">
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 no-padding">
				<div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gal-item">	
				      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
				      	<div class="box">
					      	<a href="#" data-toggle="modal" data-target="#1">
						      <img style="width: 100%;" src="./images/imagen-6-600x400.jpg" class="img-responsive">
							</a>

						    <div class="modal fade" id="1" tabindex="-1" role="dialog">
						      <div class="modal-dialog" role="document">
						        <div class="modal-content">
						            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						          <div class="modal-body">
						            <img src="./images/imagen-6-600x400.jpg">
						          </div>
						        </div>
						      </div>
						    </div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div>
					<div class="hidden-xs hidden-sm hidden-md">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gal-item">
					      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
					      <div class="box">
						      	<a href="#" data-toggle="modal" data-target="#2">
							      <img style="width: 100%;" src="./images/imagen-7-600x400.jpg" class="img-responsive">	
								</a>

						        <div class="modal fade" id="2" tabindex="-2" role="dialog">
						          <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						              <div class="modal-body">
						                <img src="./images/imagen-7-600x400.jpg">
						              </div>
						            </div>
						          </div>
						        </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 hidden-xs hidden-sm hidden-md no-padding gal-item">
				<div class="">
			      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
			     	<div class="box">
				      	<a href="#" data-toggle="modal" data-target="#3">
				      		<img style="width: 100%;" src="./images/imagen-9-600x800.jpg" class="img-responsive">
						</a>

				        <div class="modal fade" id="3" tabindex="-3" role="dialog">
				          <div class="modal-dialog" role="document">
				            <div class="modal-content">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				              <div class="modal-body">
				                <img src="./images/imagen-9-600x800.jpg">
				              </div>
				            </div>
				          </div>
				        </div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 no-padding">
				<div class="hidden-xs hidden-sm hidden-md gal-item">
			      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
			      <div class="box">
				      	<a href="#" data-toggle="modal" data-target="#4">
				      		<img style="width: 100%;" src="./images/imagen-8-600x400.jpg" class="img-responsive">
						</a>

				        <div class="modal fade" id="4" tabindex="-4" role="dialog">
				          <div class="modal-dialog" role="document">
				            <div class="modal-content">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				              <div class="modal-body">
				                <img src="./images/imagen-8-600x400.jpg">
				              </div>
				            </div>
				          </div>
				        </div>
					</div>
				</div>

				<div class="">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding gal-item">
				      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
				      	<div class="box">
				      		<a href="#" data-toggle="modal" data-target="#5">
					      	<img style="width: 100%;" src="./images/imagen-10-300x400.jpg" class="img-responsive">
							</a>

					        <div class="modal fade" id="5" tabindex="-1" role="dialog">
					          <div class="modal-dialog" role="document">
					            <div class="modal-content">
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					              <div class="modal-body">
					                <img src="./images/imagen-10-300x400.jpg">
					              </div>
					            </div>
					          </div>
					        </div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding gal-item">
						<!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
				      	<div class="box">
				      		<a href="#" data-toggle="modal" data-target="#6">
				      		<img style="width: 100%;" src="./images/imagen-11-300x400.jpg" class="img-responsive">
							</a>

					        <div class="modal fade" id="6" tabindex="-1" role="dialog">
					          <div class="modal-dialog" role="document">
					            <div class="modal-content">
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					              <div class="modal-body">
					                <img src="./images/imagen-11-300x400.jpg">
					              </div>
					            </div>
					          </div>
					        </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Final de la primera sección de la galería  -->
		<div  style="display: inline-block; float: none;">
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 no-padding">
				<div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gal-item">	
				      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
				      	<div class="box">
					      	<a href="#" data-toggle="modal" data-target="#1">
						      <img style="width: 100%;" src="./images/imagen-6-600x400.jpg" class="img-responsive">
							</a>

						    <div class="modal fade" id="1" tabindex="-1" role="dialog">
						      <div class="modal-dialog" role="document">
						        <div class="modal-content">
						            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						          <div class="modal-body">
						            <img src="./images/imagen-6-600x400.jpg">
						          </div>
						        </div>
						      </div>
						    </div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div>
					<div class="hidden-xs hidden-sm hidden-md">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding gal-item">
					      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
					      <div class="box">
						      	<a href="#" data-toggle="modal" data-target="#2">
							      <img style="width: 100%;" src="./images/imagen-7-600x400.jpg" class="img-responsive">	
								</a>

						        <div class="modal fade" id="2" tabindex="-2" role="dialog">
						          <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						              <div class="modal-body">
						                <img src="./images/imagen-7-600x400.jpg">
						              </div>
						            </div>
						          </div>
						        </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 hidden-xs hidden-sm hidden-md no-padding gal-item">
				<div class="">
			      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
			     	<div class="box">
				      	<a href="#" data-toggle="modal" data-target="#3">
				      		<img style="width: 100%;" src="./images/imagen-9-600x800.jpg" class="img-responsive">
						</a>

				        <div class="modal fade" id="3" tabindex="-3" role="dialog">
				          <div class="modal-dialog" role="document">
				            <div class="modal-content">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				              <div class="modal-body">
				                <img src="./images/imagen-9-600x800.jpg">
				              </div>
				            </div>
				          </div>
				        </div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 no-padding">
				<div class="hidden-xs hidden-sm hidden-md gal-item">
			      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
			      <div class="box">
				      	<a href="#" data-toggle="modal" data-target="#4">
				      		<img style="width: 100%;" src="./images/imagen-8-600x400.jpg" class="img-responsive">
						</a>

				        <div class="modal fade" id="4" tabindex="-4" role="dialog">
				          <div class="modal-dialog" role="document">
				            <div class="modal-content">
				                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				              <div class="modal-body">
				                <img src="./images/imagen-8-600x400.jpg">
				              </div>
				            </div>
				          </div>
				        </div>
					</div>
				</div>

				<div class="">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding gal-item">
				      <!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
				      	<div class="box">
				      		<a href="#" data-toggle="modal" data-target="#5">
					      	<img style="width: 100%;" src="./images/imagen-10-300x400.jpg" class="img-responsive">
							</a>

					        <div class="modal fade" id="5" tabindex="-1" role="dialog">
					          <div class="modal-dialog" role="document">
					            <div class="modal-content">
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					              <div class="modal-body">
					                <img src="./images/imagen-10-300x400.jpg">
					              </div>
					            </div>
					          </div>
					        </div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding gal-item">
						<!-- <img style="width: 100%;" src="http://ximg.es/1280x720/ddd/000" class="img-responsive"> -->
				      	<div class="box">
				      		<a href="#" data-toggle="modal" data-target="#6">
				      		<img style="width: 100%;" src="./images/imagen-11-300x400.jpg" class="img-responsive">
							</a>

					        <div class="modal fade" id="6" tabindex="-1" role="dialog">
					          <div class="modal-dialog" role="document">
					            <div class="modal-content">
					                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					              <div class="modal-body">
					                <img src="./images/imagen-11-300x400.jpg">
					              </div>
					            </div>
					          </div>
					        </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Final de la segunda sección de la galería -->
	</div>
</div>
<div class="clearfix"></div>

<div id="section-contacto" class="col-lg-12">
	<p class="text-center font-regular lead">Contactanos</p>
	<div class="row contact-wrap"> 
        <div class="col-md-6 col-md-offset-3">
            <form action="send.php" id="ContactForm" method="post" role="form" class="contactForm" >
                <div class="form-group font-regular text-muted">
                	<label for="nombre">Nombre (requerido)</label>
                    <input type="text" name="nombre" class="form-control background-input-contact" id="nombre" placeholder="Su Nombre">
                </div>
                <div class="form-group font-regular text-muted">
                	<label for="correo">Correo eletrónico (requerido)</label>
                    <input type="email" class="form-control background-input-contact" name="correo" id="correo" placeholder="Su Correo">
                </div>
                <div class="form-group font-regular text-muted">
                	<label for="asunto">Asunto</label>
                    <input type="text" class="form-control background-input-contact" name="asunto" id="asunto" placeholder="Asunto">
                </div>
                <div class="form-group font-regular text-muted">
                <label for="mensaje">Mensaje</label>
                    <textarea style="resize: none;" class="form-control background-input-contact" id="mensaje" name="mensaje" rows="5" placeholder="Mensaje"></textarea>
                </div>
            	<div class="form-group">
                    <div class="g-recaptcha" data-sitekey="6LdTFCYUAAAAAPzhESb9VHyzYa0eny_4KtHnaOBv"></div>
                </div>
                <div class="form-group">
                    <button type="button" id="spinnerContacto" class="btn" disabled="disabled" style="display: none;"><span class="fa fa-2x fa-spinner fa-spin"></span></button>
                    <button type="submit" id="buttonContacto" name="submit" class="btn btn-contact" required="required">Enviar</button>
                </div>
            </form>                       
        </div>
    </div>
</div>

<!-- FOOTER -->
<footer class="footer">
  <div class="container">
    <div class="col-md-8 col-sm-12 col-xm-12">
		<div class="center-block col-md-12 col-sm-12 col-xm-12" style="width: 100%; margin-top: 8%;">
			<div id="direccion-footer" class="row" style="margin-left: -30%;">
				<div class="col-md-4 col-sm-12 col-xm-12 no-padding" style="margin-top: -1%;">
					<img id="icono-direccion-footer" src="images/gps.png" height="40">
				</div>
		    	<div class="col-md-8 col-sm-12 col-xm-12">
		    		<p class="text-muted">Urb. San José de Tarbes, Av. Pase Cabriales Torre BOD, Pb, Local P-42. <br>Teléfono: +58 (241) 825-8789 - contacto@dralilianherrera.com <br><small>Rif: J40060780-9</small></p>
		    	</div>	
    		</div>
    	</div>
    </div>
	<div class="col-lg-4 col-md-4 col-sm-12 col-xm-12">
	  	<div id="iconos-social-footer" class="text-center" style=" margin-top: 18%;">
	  		<img src="images/red-heart.png" height="40" class="img-circle">
	  		<img src="images/twitter-logo.png" height="25" class="img-circle">
	  		<img src="images/instagram-logo.png" height="25" class="img-circle">
	  		<img src="images/facebook-logo.svg" height="25" class="img-circle">
	  	</div>
	</div>
  </div>
</footer>

<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/lian-herrera.js"></script>
<script type="text/javascript" src="js/galeria.js"></script>

</body>
</html>
